import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class Item {

    public static final float FONT_SIZE = 40;
    public static final Dimension SIZE = new Dimension(50, 50);

    private final int number;
    private double animationFrame = 0;

    public Item(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public double getAnimationFrame() {
        return animationFrame;
    }

    public void addAnimationFrames(double amount) {
        animationFrame += amount;
    }

    public  void resetAnimationFrame() {
        animationFrame = 0;
    }

    public void draw(Graphics2D graphics, Point position) {
        graphics.setColor(NordTheme.colors[1]);
        graphics.fillRect(position.x, position.y, SIZE.width, SIZE.height);

        String text = Integer.toString(number);
        graphics.setColor(NordTheme.colors[6]);
        graphics.setFont(graphics.getFont().deriveFont(FONT_SIZE));
        FontMetrics fontMetrics = graphics.getFontMetrics();
        Rectangle2D stringBounds = fontMetrics.getStringBounds(text, graphics);

        graphics.drawString(Integer.toString(number), position.x + SIZE.width / 2 - (int) stringBounds.getCenterX(), position.y + SIZE.height / 2 - (int) stringBounds.getCenterY());
    }
}