import java.awt.Color;

public class NordTheme {
    public static final Color[] colors = {
        new Color(0x2e, 0x34, 0x40),
        new Color(0x3b, 0x42, 0x52),
        new Color(0x43, 0x4c, 0x5e),
        new Color(0x4c, 0x56, 0x6a),

        new Color(0xd8, 0xde, 0xe9),
        new Color(0xe5, 0xe9, 0xf0),
        new Color(0xec, 0xef, 0xf4),
    };
}
