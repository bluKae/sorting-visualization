import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.Random;

public class SortingPanel extends JPanel implements Drawable {

    private final Item[] items = new Item[10];

    private final Random random = new Random();

    public SortingPanel() {
        setSize(new Dimension(Item.SIZE.width * items.length, Item.SIZE.height));
        randomizeItems();
    }

    public void randomizeItems() {
        synchronized (items) {
            Arrays.setAll(items, i -> new Item(random.nextInt(10)));
        }

        repaint();
    }

    public void unsort() {
        for (int i = 0; i < items.length; i++) {
             swapItems(random.nextInt(items.length), random.nextInt(items.length));
        }
    }

    public void bubbleSort() {
        boolean anyChanges = true;

        for (int i = items.length - 1; i > 0 && anyChanges; i--) {
            anyChanges = false;
            for (int j = 0; j < i; j++) {
                if (items[j].getNumber() > items[j + 1].getNumber()) {
                    swapItems(j, j + 1);
                    anyChanges = true;
                }
            }
        }
    }

    public void selectionSort() {
        for (int i = 0; i < items.length; i++) {
            int minIndex = i;
            for (int j = i; j < items.length; j++) {
                if (items[j].getNumber() < items[minIndex].getNumber()) {
                    minIndex = j;
                }
            }
            swapItems(i, minIndex);
        }
    }

    public void insertionSort() {
        for (int i = 1; i < items.length; i++) {
            for (int j = i - 1; j >= 0 && items[j].getNumber() > items[j + 1].getNumber(); j--) {
                swapItems(j, j + 1);
            }
        }
    }

    private void swapItems(int indexOfItem1, int indexOfItem2) {
        int numberOfSteps = 10;
        int distanceBetweenItems = Math.abs(indexOfItem1 - indexOfItem2);

        for(int i = 0; i < numberOfSteps; i++) {
            synchronized (items) {
                items[indexOfItem1].addAnimationFrames((double) 1 / numberOfSteps * distanceBetweenItems);
                items[indexOfItem2].addAnimationFrames((double) -1 / numberOfSteps * distanceBetweenItems);
            }

            SwingUtilities.invokeLater(this::repaint);
            try {
                Thread.sleep(250L / numberOfSteps);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        synchronized (items) {
            Item tmp = items[indexOfItem1];
            items[indexOfItem1] = items[indexOfItem2];
            items[indexOfItem2] = tmp;

            items[indexOfItem1].resetAnimationFrame();
            items[indexOfItem2].resetAnimationFrame();
        }
    }

    @Override
    public void draw(Graphics2D g) {
        g.setBackground(NordTheme.colors[0]);
        g.clearRect(0, 0, getWidth(), getHeight());

        synchronized (items) {
            for (int i = 0; i < items.length; i++) {
                items[i].draw(g, getItemPosition(i + items[i].getAnimationFrame()));
            }
        }
    }

    private Point getItemPosition(double indexOfItem) {
        return new Point(
                (int) (getWidth() / 2 - items.length * Item.SIZE.width / 2 + indexOfItem * Item.SIZE.width),
                getHeight() / 2 - Item.SIZE.height / 2
        );
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        draw((Graphics2D) g);
    }
}
