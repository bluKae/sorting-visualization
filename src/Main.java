import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Arrays;

public class Main {

    public static final float FONT_SIZE = 30;

    private static JButton createButton(String text, ActionListener listener) {

        JButton button = new JButton(text);

        button.addActionListener(listener);
        button.setBackground(NordTheme.colors[1]);

        button.setBorder(new EmptyBorder(0, 0, 0, 0));
        button.setFont(button.getFont().deriveFont(FONT_SIZE));
        button.setForeground(NordTheme.colors[6]);
        button.setFocusPainted(false);
        return button;
    }

    private static JPanel createHorizontalPanel(int width, JComponent... components) {
        GridLayout layout = new GridLayout(0, components.length);
        JPanel panel = new JPanel(layout);
        panel.setSize(width, 20);

        Arrays.stream(components).forEach(panel::add);

        return panel;
    }

    public static void main(String[] args) {
        SortingPanel sortingPanel = new SortingPanel();

        JPanel buttonPanel = createHorizontalPanel(
                sortingPanel.getWidth(),
                createButton("Bubble-sort", event -> new Thread(sortingPanel::bubbleSort).start()),
                createButton("Selection", event -> new Thread(sortingPanel::selectionSort).start()),
                createButton("Insertions", event -> new Thread(sortingPanel::insertionSort).start()),
                createButton("Randomize", event -> {
                    sortingPanel.randomizeItems();
                    // new Thread(sortingPanel::unsort).start();
                })
        );


        JFrame window = new JFrame("Sorting");

        window.setSize(sortingPanel.getWidth() + 200, sortingPanel.getHeight() + buttonPanel.getHeight() + 200);

        window.add(sortingPanel, BorderLayout.CENTER);
        window.add(buttonPanel, BorderLayout.SOUTH);

        window.setLocationRelativeTo(null);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setResizable(true);
        window.setVisible(true);
    }
}